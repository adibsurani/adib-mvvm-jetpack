package com.adibsurani.aerotask.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.adibsurani.aerotask.model.auth.LoginModel;
import com.adibsurani.aerotask.repository.LoginRepository;

public class LoginViewModel extends ViewModel {

    private MutableLiveData<LoginModel> loginModel;
    private LoginRepository loginRepository;

    public LoginViewModel() {
        loginRepository = new LoginRepository();
    }

    public MutableLiveData<LoginModel> postLogin(MutableLiveData<String> identifier,
                                                 MutableLiveData<String> password) {
        loginModel = loginRepository.postLogin(identifier, password);
        return this.loginModel;
    }

    public MutableLiveData<String> postLoginError() {
        return loginRepository.postLoginError();
    }
}
