package com.adibsurani.aerotask.ui.activity

import com.adibsurani.aerotask.R
import com.adibsurani.aerotask.ui.base.BaseActivity

class OnboardActivity: BaseActivity() {

    override fun initLayout(): Int {
        return R.layout.activity_onboard
    }

    override fun initCreate() {

    }

}