package com.adibsurani.aerotask.model.auth

data class UserModel(val _id: String,
                     val username: String,
                     val email: String)