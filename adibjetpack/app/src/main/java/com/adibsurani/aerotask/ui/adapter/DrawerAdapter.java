package com.adibsurani.aerotask.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.adibsurani.aerotask.R;
import com.adibsurani.aerotask.model.DrawerModel;
import com.adibsurani.aerotask.ui.activity.HomeActivity;

import java.util.ArrayList;

public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.ViewHolder> {

    private ArrayList<DrawerModel> drawerItem;
    private Context context;
    private HomeActivity activity;
    private int selectedPosition = 0;

    public DrawerAdapter(Context context,
                         Activity activity) {
        this.context = context;
        this.activity = (HomeActivity) context;
        this.drawerItem = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_drawer, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final DrawerModel drawer = drawerItem.get(position);
        holder.layoutBase.setBackgroundColor(selectedPosition == position? context.getResources().getColor(R.color.green_900) : context.getResources().getColor(R.color.colorPrimary));
        holder.imageIcon.setImageResource(drawer.getIcon());
        holder.textTitle.setText(drawer.getTitle());
    }

    @Override
    public int getItemCount() {
        return this.drawerItem.size();
    }

    public void setDataSource(ArrayList<DrawerModel> drawerItem) {
        this.drawerItem.addAll(drawerItem);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textTitle;
        private ImageView imageIcon;
        private RelativeLayout layoutBase;

        ViewHolder(View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.text_title);
            imageIcon = itemView.findViewById(R.id.image_icon);
            layoutBase = itemView.findViewById(R.id.layout_base);

            itemView.setOnClickListener((v) -> {
                if (getAdapterPosition() == RecyclerView.NO_POSITION) {
                    return;
                }
                notifyItemChanged(selectedPosition);
                selectedPosition = getAdapterPosition();
                notifyItemChanged(selectedPosition);
                activity.selectDrawer(getAdapterPosition());
            });
        }
    }
}
