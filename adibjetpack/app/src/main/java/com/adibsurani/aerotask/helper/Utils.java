package com.adibsurani.aerotask.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Base64;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.POWER_SERVICE;

public class Utils {

    public static final String TAG = "Utils";

    public static boolean isInternetConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService
                (Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        boolean isConnected = networkInfo != null && networkInfo.isConnectedOrConnecting();
        return isConnected;

    }

    public static boolean isGpsEnabled(Context context) {
        final LocationManager manager = (LocationManager) context.getSystemService(Context
                .LOCATION_SERVICE);

        boolean isGpsEnable = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        return isGpsEnable;
    }

    public static boolean emailValidation(String emailstring) {
        if (null == emailstring || emailstring.length() == 0) {
            return false;
        }
        Pattern emailPattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher emailMatcher = emailPattern.matcher(emailstring);
        return emailMatcher.matches();
    }

    public static void showToast(String message, Context context) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static boolean isBlank(String value) {
        return value == null || value.trim().length() == 0;
    }

    public static boolean hasAnyPrefix(String number, String... prefixes) {
        if (number == null) {
            return false;
        }
        for (String prefix : prefixes) {
            if (number.startsWith(prefix)) {
                return true;
            }
        }
        return false;
    }

    public static String convertImageToBase64(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.URL_SAFE);

        return encodedImage;
    }

    public static String getDayOfMonthSuffix(final int n) {
        if (n >= 11 && n <= 13) {
            return n + "th";
        }
        switch (n % 10) {
            case 1:
                return n + "st";
            case 2:
                return n + "nd";
            case 3:
                return n + "rd";
            default:
                return n + "th";
        }
    }

    public static String trimString(String address) {
        boolean isInteger;
        String original = "";
        if (!address.isEmpty()) {
            String[] strings = address.split(",");
            String s = address.substring(0, 1);
            try {
                Integer.valueOf(s);
                isInteger = true;
            } catch (NumberFormatException e) {
                isInteger = false;
            }

            int stringLength = strings.length;
            AppLog.Companion.infoLog("String Length", stringLength + "");

            if (isInteger) {
                switch (stringLength) {
                    case 1:
                    case 2:
                        original = address;
                        break;
                    default:
                        original += strings[0] + "," + strings[1];
                        break;
                }
            } else {
                switch (stringLength) {
                    case 1:
                        original = address;
                        break;
                    case 2:
                        original = strings[0];
                        break;
                    default:
                        original = strings[0] + "," + strings[1];
                        break;
                }
            }
        }
        return original.trim();
    }

    public static boolean isScreenOn(Context context) {
        PowerManager powerManager = (PowerManager) context.getSystemService(POWER_SERVICE);
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH
                ? powerManager.isInteractive()
                : powerManager.isScreenOn();
    }

    public static String secondsToHoursMinutesSeconds(long seconds) {
        return (seconds / 3600) + " hr" + " " + (seconds % 3600) / 60 + " min";
    }

    public static String fahrenheitToCelsius(int temperature) {
        return ((temperature - 32)*5)/9 + "ºC";
    }

    public static void closeKeyboard(Context c, IBinder windowToken) {
        InputMethodManager mgr = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(windowToken, 0);
    }
}


