package com.adibsurani.aerotask.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.adibsurani.aerotask.R;
import com.adibsurani.aerotask.model.TaskModel;

import java.util.ArrayList;
import java.util.List;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.ViewHolder> {

    private List<TaskModel> taskList;
    private Context context;
    private Activity activity;
    private TaskClickListener taskClickListener;
    private int savePosition;

    public TaskAdapter(Context context, Activity activity) {
        this.context = context;
        this.activity = (Activity) context;
        this.taskList = new ArrayList<>();
    }

    public interface TaskClickListener {
        void onTaskClick(int position,
                         TaskModel taskModel);
    }

    public void setTaskClickListener(TaskAdapter.TaskClickListener taskClickListener) {
        this.taskClickListener = taskClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_task, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder,
                                 int position) {
        final TaskModel task = taskList.get(position);
        switch (task.getStatus()) {
            case "In Progress":
                holder.layoutStatus.setBackground(context.getDrawable(R.drawable.layout_rounded_progress));
                break;
            case "Open":
                holder.layoutStatus.setBackground(context.getDrawable(R.drawable.layout_rounded_open));
                break;
            case "Complete":
                holder.layoutStatus.setBackground(context.getDrawable(R.drawable.layout_rounded_complete));
                break;
        }
        holder.textTitle.setText(task.getName());
        holder.textDescription.setText(task.getDescription());
    }

    @Override
    public int getItemCount() {
        return this.taskList.size();
    }

    public void setDataSource(List<TaskModel> taskList) {
        this.taskList.addAll(taskList);
        notifyDataSetChanged();
    }

    public List<TaskModel> taskList() {
        return this.taskList;
    }

    public void removeTask(String taskID) {
        for (int i=0; i<this.taskList.size(); i++) {
            TaskModel task = this.taskList.get(i);
            if (taskID == task.get_id()) {
                this.taskList.remove(task);
                savePosition = i;
            }
        }
    }

    public void updateTask(TaskModel taskModel) {
        this.taskList.add(savePosition, taskModel);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private LinearLayout layoutStatus;
        private TextView textTitle, textDescription;

        ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            layoutStatus = itemView.findViewById(R.id.layout_status);
            textTitle = itemView.findViewById(R.id.text_title);
            textDescription = itemView.findViewById(R.id.text_description);
        }

        @Override
        public void onClick(View v) {
            TaskModel task = taskList.get(getAdapterPosition());
            if (taskClickListener != null) {
                taskClickListener.onTaskClick(getAdapterPosition(), task);
            }
        }
    }
}
