package com.adibsurani.aerotask.helper

class Constants {

    companion object {
        // general
        const val BASE_URL          : String = "https://19a1f6a4.ngrok.io/"
        const val BASE_WEATHER_URL  : String = "https://api.darksky.net/forecast/c106efb953b9e132706c3fea37078791/"
        const val PREF_NAME         : String = "AEROTASK"

        // preferences
        const val FIRST_RUN         : String = "firstrun"
        const val IS_LOGIN          : String = "IS_LOGIN"
        const val USER_ID           : String = "USER_ID"
        const val USERNAME          : String = "USERNAME"
        const val EMAIL             : String = "EMAIL"
        const val TOKEN             : String = "TOKEN"

        // login state
        const val LOGIN_DEFAULT     : Int = 0
        const val LOGIN_LOADING     : Int = 1
        const val LOGIN_OK          : Int = 2
        const val LOGIN_ERROR       : Int = 3
    }
}