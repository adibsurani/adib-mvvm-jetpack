package com.adibsurani.aerotask.ui.base

import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.adibsurani.aerotask.helper.PrefManager
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger

abstract class BaseFragment: Fragment() {

    val loadHandler = Handler()
    lateinit var prefManager: PrefManager
    private var viewFragment: View? = null
    private var layoutInflaterFragment: LayoutInflater? = null

    protected abstract fun initLayout(): Int
    protected abstract fun initCreated()
    protected abstract fun initBack()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prefManager = PrefManager(context!!)
        Logger.addLogAdapter(AndroidLogAdapter())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewFragment = inflater.inflate(initLayout(), container, false)
        layoutInflaterFragment = LayoutInflater.from(activity)
        return viewFragment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initCreated()
    }

    override fun onResume() {
        super.onResume()
        if (view == null) {
            initCreated()
            return
        }
        view!!.isFocusableInTouchMode = true
        view!!.requestFocus()
        view!!.setOnKeyListener {_, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP
                && keyCode == KeyEvent.KEYCODE_BACK) {
                initBack()
                return@setOnKeyListener true
            }
            false
        }
    }

}