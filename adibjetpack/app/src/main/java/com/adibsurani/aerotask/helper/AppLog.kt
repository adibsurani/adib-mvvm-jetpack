package com.adibsurani.aerotask.helper

import android.util.Log

class AppLog {

    companion object {
        fun debugLog(tag: String, message: String) {
            Log.d(tag, message + "")
        }

        fun infoLog(tag: String, message: String) {
            Log.i(tag, message + "")
        }

        fun errorLog(tag: String, message: String) {
            Log.e(tag, message + "")
        }
    }
}