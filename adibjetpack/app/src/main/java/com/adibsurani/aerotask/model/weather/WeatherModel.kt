package com.adibsurani.aerotask.model.weather

data class WeatherModel(val latitude: Double,
                        val longitude: Double,
                        val timezone: String,
                        val currently: CurrentModel,
                        val daily: DailyModel)