package com.adibsurani.aerotask.network

import com.adibsurani.aerotask.helper.Constants.Companion.BASE_URL
import com.adibsurani.aerotask.model.TaskModel
import com.adibsurani.aerotask.model.UpdateTaskModel
import com.adibsurani.aerotask.model.auth.LoginModel
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface APIService {

    companion object {
        fun create(): APIService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(APIService::class.java)
        }
    }

    /*-----POST-----*/
    // login
    @POST("auth/local")
    @FormUrlEncoded
    fun postLogin(@Field("identifier") username: String,
                  @Field("password") password: String): Single<LoginModel>

    // create task
    @POST("tasks")
    @FormUrlEncoded
    fun postCreateTask(@Field("name") name: String,
                       @Field("description") description: String,
                       @Field("order") order: Int,
                       @Field("status") status: String): Observable<TaskModel>

    /*-----GET-----*/
    // list tasks
    @GET("tasks")
    fun getTasksList(@Header("Authorization") token: String): Observable<List<TaskModel>>

    // list tasks count
    @GET("tasks/count")
    fun getTasksListCount(): Observable<Int>

    // show task
    @GET("tasks/{taskID}")
    fun getTask(@Path("taskID") taskID: String,
                @Header("Authorization") token: String): Observable<TaskModel>

    /*-----PUT-----*/
    // update task
    @PUT("tasks/{taskID}")
    fun updateTask(@Header("Authorization") token: String,
                   @Path("taskID") taskID: String,
                   @Body updateTaskModel: UpdateTaskModel): Observable<TaskModel>

    /*-----DELETE-----*/
    // delete task
    @DELETE("tasks/{taskID}")
    fun deleteTask(@Path("taskID") taskID: String): Single<TaskModel>
}