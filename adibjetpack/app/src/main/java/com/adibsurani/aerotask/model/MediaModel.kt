package com.adibsurani.aerotask.model

data class MediaModel(val _id: String,
                      val name: String,
                      val ext: String,
                      val size: String,
                      val url: String)