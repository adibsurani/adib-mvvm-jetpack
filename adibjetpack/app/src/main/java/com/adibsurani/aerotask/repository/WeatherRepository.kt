package com.adibsurani.aerotask.repository

import androidx.lifecycle.MutableLiveData
import com.adibsurani.aerotask.model.weather.WeatherModel
import com.adibsurani.aerotask.network.APIWeatherService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class WeatherRepository {

    private val subscription = CompositeDisposable()
    private val api: APIWeatherService = APIWeatherService.create()
    private val getWeatherError = MutableLiveData<String>()

    fun getWeather(latitude: Double,
                   longitude: Double): MutableLiveData<WeatherModel> {
        val weatherResponse = MutableLiveData<WeatherModel>()
        val subscribe = api
            .getWeather(latitude, longitude)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                weatherResponse.value = it
            }, {
                getWeatherError.value = it.message
            })
        subscription.add(subscribe)
        return  weatherResponse
    }

    fun getWeatherError(): MutableLiveData<String> {
        return getWeatherError
    }
}