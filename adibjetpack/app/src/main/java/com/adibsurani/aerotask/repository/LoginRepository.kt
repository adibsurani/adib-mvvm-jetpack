package com.adibsurani.aerotask.repository

import androidx.lifecycle.MutableLiveData
import com.adibsurani.aerotask.model.auth.LoginModel
import com.adibsurani.aerotask.network.APIService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class LoginRepository {

    private val subscription = CompositeDisposable()
    private val api: APIService = APIService.create()
    var loginError = MutableLiveData<String>()

    fun postLogin(userID: MutableLiveData<String>,
                  password: MutableLiveData<String> ): MutableLiveData<LoginModel> {
        val loginResponse = MutableLiveData<LoginModel>()
        val subscribe = api.postLogin(userID.value!!, password.value!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loginResponse.value = it
            }, {
                loginError.value = it.localizedMessage
            })
        subscription.add(subscribe)
        return loginResponse
    }

    fun postLoginError(): MutableLiveData<String> {
        return loginError
    }
}