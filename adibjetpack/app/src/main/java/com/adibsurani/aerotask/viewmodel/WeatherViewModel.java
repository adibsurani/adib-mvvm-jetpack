package com.adibsurani.aerotask.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.adibsurani.aerotask.model.weather.WeatherModel;
import com.adibsurani.aerotask.repository.WeatherRepository;

public class WeatherViewModel extends ViewModel {

    private MutableLiveData<WeatherModel> weatherModel;
    private WeatherRepository weatherRepository;

    public WeatherViewModel() {
        weatherRepository = new WeatherRepository();
    }

    public MutableLiveData<WeatherModel> getWeather(Double latitude,
                                                    Double longitude) {
        weatherModel = weatherRepository
                .getWeather(latitude, longitude);
        return this.weatherModel;
    }

    public MutableLiveData<String> getWeatherError() {
        return weatherRepository.getWeatherError();
    }
}
