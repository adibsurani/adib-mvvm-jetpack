package com.adibsurani.aerotask.model.auth

data class LoginModel(val jwt: String,
                      val user: UserModel
)