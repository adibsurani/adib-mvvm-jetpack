package com.adibsurani.aerotask.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.adibsurani.aerotask.model.TaskModel;
import com.adibsurani.aerotask.model.UpdateTaskModel;
import com.adibsurani.aerotask.repository.TaskRepository;

import java.util.List;

public class TaskViewModel extends ViewModel {

    private MutableLiveData<List<TaskModel>> allTasksModel;
    private MutableLiveData<TaskModel> taskModel;
    private TaskRepository taskRepository;

    public TaskViewModel() {
        taskRepository = new TaskRepository();
    }

    // GET all tasks
    public MutableLiveData<List<TaskModel>> getAllTasks() {
        allTasksModel = taskRepository.getAllTasks();
        return this.allTasksModel;
    }

    public MutableLiveData<String> allTasksError() {
        return taskRepository.getAllTaskError();
    }

    // GET task by ID
    public MutableLiveData<TaskModel> getTask(String taskID) {
        taskModel = taskRepository.getTask(taskID);
        return this.taskModel;
    }

    public MutableLiveData<String> getTaskError() {
        return taskRepository.getTaskError();
    }

    // PUT update task by ID
    public MutableLiveData<TaskModel> updateTask(String taskID,
                                                 UpdateTaskModel updateTaskModel) {
        taskModel = taskRepository.updateTask(
                        taskID,
                        updateTaskModel);
        return this.taskModel;
    }

    public MutableLiveData<String> updateTaskError() {
        return taskRepository.updateTaskError();
    }

}
