package com.adibsurani.aerotask.network

import com.adibsurani.aerotask.helper.Constants
import com.adibsurani.aerotask.model.weather.WeatherModel
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface APIWeatherService {

    companion object {
        fun create(): APIWeatherService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constants.BASE_WEATHER_URL)
                .build()
            return retrofit.create(APIWeatherService::class.java)
        }
    }

    /*-----GET-----*/
    // weather
    @GET("{latitude},{longitude}")
    fun getWeather(@Path("latitude") latitude: Double,
                   @Path("longitude") longitude: Double): Observable<WeatherModel>


}