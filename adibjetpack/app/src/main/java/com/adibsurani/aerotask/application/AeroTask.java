package com.adibsurani.aerotask.application;

import android.app.Application;
import android.content.Context;

public class AeroTask extends Application {
    private static Context context;

    public void onCreate() {
        super.onCreate();
        AeroTask.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return AeroTask.context;
    }
}
