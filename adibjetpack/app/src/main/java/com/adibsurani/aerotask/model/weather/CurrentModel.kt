package com.adibsurani.aerotask.model.weather

data class CurrentModel(val time: Long,
                        val summary: String,
                        val icon: String,
                        val temperature: Double)