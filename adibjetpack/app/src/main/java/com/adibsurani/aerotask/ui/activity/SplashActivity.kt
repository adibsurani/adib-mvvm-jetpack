package com.adibsurani.aerotask.ui.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.adibsurani.aerotask.R
import com.adibsurani.aerotask.helper.Constants
import com.adibsurani.aerotask.helper.Constants.Companion.PREF_NAME
import com.adibsurani.aerotask.ui.base.BaseActivity
import qiu.niorgai.StatusBarCompat

@SuppressLint("CommitPrefEdits")
class SplashActivity: BaseActivity() {

    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor

    override fun initLayout(): Int {
        return R.layout.activity_splash
    }

    override fun initCreate() {
        StatusBarCompat.setStatusBarColor(this, resources.getColor(R.color.colorPrimary))
        initView()
        initData()
    }

    private fun initView() {

    }

    private fun initData() {
        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        checkFirstRun()
    }

    private fun checkFirstRun() {
        if (sharedPreferences.getBoolean(Constants.FIRST_RUN, true)) {
            editor.putBoolean(Constants.FIRST_RUN, false).apply()
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in,R.anim.fade_out)
            finish()
        } else {
            checkAuthentication()
        }
    }

    private fun checkAuthentication() {
        loadHandler.postDelayed({
            if (pref.isLoggedIn) {
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in,R.anim.fade_out)
                finish()
            } else {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in,R.anim.fade_out)
                finish()
            }
        }, 500)
    }
}