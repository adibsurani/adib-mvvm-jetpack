package com.adibsurani.aerotask.model

data class DrawerModel(val icon: Int,
                       val title: String)