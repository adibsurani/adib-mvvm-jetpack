package com.adibsurani.aerotask.ui.activity

import android.content.Intent
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.adibsurani.aerotask.R
import com.adibsurani.aerotask.helper.AppLog
import com.adibsurani.aerotask.helper.Constants.Companion.LOGIN_DEFAULT
import com.adibsurani.aerotask.helper.Constants.Companion.LOGIN_ERROR
import com.adibsurani.aerotask.helper.Constants.Companion.LOGIN_LOADING
import com.adibsurani.aerotask.helper.Constants.Companion.LOGIN_OK
import com.adibsurani.aerotask.helper.Utils.emailValidation
import com.adibsurani.aerotask.helper.Utils.isInternetConnected
import com.adibsurani.aerotask.model.auth.LoginModel
import com.adibsurani.aerotask.ui.base.BaseActivity
import com.adibsurani.aerotask.viewmodel.LoginViewModel
import com.jaeger.library.StatusBarUtil
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity: BaseActivity() {

    private lateinit var viewModel: LoginViewModel
    private val liveUsername = MutableLiveData<String>()
    private val livePassword = MutableLiveData<String>()

    override fun initLayout(): Int {
        return R.layout.activity_login
    }

    override fun initCreate() {
        StatusBarUtil.setTransparent(this)
        initView()
        initData()
    }

    private fun initView() {
        setupClick()
    }

    private fun initData() {

    }

    private fun setupLogin() {
        if (!isInternetConnected(this)) {
            AppLog.errorLog("NETWORK", "No Connection")
            loginState(LOGIN_ERROR, "No Connection")
            return
        }
        if (edit_username.text.isEmpty() || edit_password.text.isEmpty()) {
            AppLog.errorLog("LOGIN", "Form Blank")
            loginState(LOGIN_ERROR, "Form Blank")
            return
        }
        if (!emailValidation(edit_username.text.toString())) {
            AppLog.errorLog("LOGIN", "Email not valid")
            loginState(LOGIN_ERROR, "Email not valid")
            return
        }
        liveUsername.value = edit_username.text.toString()
        livePassword.value = edit_password.text.toString()
        postLogin()
    }

    private fun setupClick() {
        layout_login.setOnClickListener {
            loginState(LOGIN_LOADING, "Loading")
            it.isClickable = false
            setupLogin()
        }
    }

    private fun postLogin() {
        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        viewModel
            .postLogin(liveUsername,
                        livePassword)
            .observe(this, Observer<LoginModel> { loginModel ->
                AppLog.debugLog("LOGIN OK", "$loginModel")
                pref.setupLogin(loginModel)
                gotoHome()
                loginState(LOGIN_OK, "Login Success!")
            })
        viewModel
            .postLoginError()
            .observe(this, Observer<String> { error ->
                AppLog.errorLog("LOGIN KO", error)
                loginState(LOGIN_ERROR, error)
        })
    }

    private fun gotoHome() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.fade_in,R.anim.fade_out)
        finish()
    }

    private fun loginState(state: Int, message: String) {
        when (state) {
            LOGIN_DEFAULT -> {
                layout_login.isClickable = true
                layout_login.setBackgroundColor(resources.getColor(R.color.green_900))
                text_login.text = message
            }
            LOGIN_OK -> {
                layout_login.isClickable = true
                layout_login.setBackgroundColor(resources.getColor(R.color.blue_700))
                text_login.text = message
            }
            LOGIN_LOADING -> {
                layout_login.setBackgroundColor(resources.getColor(R.color.yellow_700))
                text_login.text = message
            }
            LOGIN_ERROR -> {
                layout_login.setBackgroundColor(resources.getColor(R.color.red_700))
                text_login.text = message
                loadHandler.postDelayed({
                    layout_login.isClickable = true
                    loginState(LOGIN_DEFAULT, "LOGIN")
                }, 1500)
            }
        }
    }
}