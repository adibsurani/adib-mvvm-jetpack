package com.adibsurani.aerotask.model

import com.adibsurani.aerotask.model.auth.UserModel

data class TaskModel(val name: String,
                     val description: String,
                     val order: Int,
                     val status: String,
                     val _id: String,
                     val updatedAt: String,
                     val user: UserModel,
                     val medias: ArrayList<MediaModel>)