package com.adibsurani.aerotask.model.weather

data class DailyModel(val summary: String,
                      val icon: String)