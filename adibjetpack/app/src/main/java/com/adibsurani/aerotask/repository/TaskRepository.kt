package com.adibsurani.aerotask.repository

import androidx.lifecycle.MutableLiveData
import com.adibsurani.aerotask.application.AeroTask
import com.adibsurani.aerotask.helper.PrefManager
import com.adibsurani.aerotask.model.TaskModel
import com.adibsurani.aerotask.model.UpdateTaskModel
import com.adibsurani.aerotask.network.APIService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class TaskRepository {

    private val prefManager: PrefManager = PrefManager(AeroTask.getAppContext())
    private val subscription = CompositeDisposable()
    private val api: APIService = APIService.create()
    private var allTasksError = MutableLiveData<String>()
    private var getTaskError = MutableLiveData<String>()
    private var updateTaskError = MutableLiveData<String>()

    fun getAllTasks(): MutableLiveData<List<TaskModel>> {
        val allTasksResponse = MutableLiveData<List<TaskModel>>()
        val subscribe = api
            .getTasksList(prefManager.bearerToken)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                allTasksResponse.value = it
            }, {
                allTasksError.value = it.message
            })
        subscription.add(subscribe)
        return allTasksResponse
    }

    fun getAllTaskError(): MutableLiveData<String> {
        return allTasksError
    }

    fun getTask(taskID: String): MutableLiveData<TaskModel> {
        val taskResponse = MutableLiveData<TaskModel>()
        val subscribe = api
            .getTask(taskID, prefManager.bearerToken)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                taskResponse.value = it
            }, {
                getTaskError.value = it.message
            })
        subscription.add(subscribe)
        return taskResponse
    }

    fun getTaskError(): MutableLiveData<String> {
        return getTaskError
    }

    fun updateTask(taskID: String,
                   updateTaskModel: UpdateTaskModel): MutableLiveData<TaskModel> {
        val taskResponse = MutableLiveData<TaskModel>()
        val subscribe = api
            .updateTask(
                prefManager.bearerToken,
                taskID,
                updateTaskModel)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                taskResponse.value = it
            }, {
                getTaskError.value = it.message
            })
        subscription.add(subscribe)
        return taskResponse
    }

    fun updateTaskError(): MutableLiveData<String> {
        return updateTaskError
    }
}