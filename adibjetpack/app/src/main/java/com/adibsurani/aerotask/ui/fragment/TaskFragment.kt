package com.adibsurani.aerotask.ui.fragment

import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.adibsurani.aerotask.R
import com.adibsurani.aerotask.helper.AnimationUtil.*
import com.adibsurani.aerotask.helper.RecyclerUtil
import com.adibsurani.aerotask.helper.Utils.closeKeyboard
import com.adibsurani.aerotask.helper.Utils.fahrenheitToCelsius
import com.adibsurani.aerotask.model.TaskModel
import com.adibsurani.aerotask.model.UpdateTaskModel
import com.adibsurani.aerotask.model.weather.WeatherModel
import com.adibsurani.aerotask.ui.activity.HomeActivity
import com.adibsurani.aerotask.ui.adapter.TaskAdapter
import com.adibsurani.aerotask.ui.base.BaseFragment
import com.adibsurani.aerotask.viewmodel.TaskViewModel
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.fragment_task.*
import kotlinx.android.synthetic.main.layout_task_detail.*
import kotlinx.android.synthetic.main.layout_weather.*

class TaskFragment: BaseFragment() {

    private lateinit var viewModel: TaskViewModel
    private lateinit var taskAdapter: TaskAdapter
    private val selectedTask = MutableLiveData<TaskModel>()
    private val taskList = ArrayList<TaskModel>()

    override fun initLayout(): Int {
        return R.layout.fragment_task
    }

    override fun initCreated() {
        initViewModel()
        initRecycler()
        initClick()
        initAdapter()
        initShimmer()
    }

    override fun initBack() {
        if (recycler_task.visibility == VISIBLE && layout_detail.visibility == GONE) {
            activity!!.finish()
            return
        }
        if (layout_detail.visibility == VISIBLE && recycler_task.visibility == GONE) {
            initAnimate()
            return
        }
        initAnimate()
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders
            .of(this)
            .get(TaskViewModel::class.java)
    }

    private fun initClick() {
        image_back.setOnClickListener {
            initAnimate()
        }
        image_add_task.setOnClickListener {
            addTask()
        }
    }

    private fun initRecycler() {
        RecyclerUtil.setupHorizontal(recycler_task, context!!)
    }

    private fun initAdapter() {
        taskAdapter = TaskAdapter(context!!, activity)
        recycler_task.adapter = taskAdapter
        taskAdapter.setTaskClickListener { _, taskModel ->
            Logger.d("$taskModel")
            initAnimate()
            selectedTask.value = taskModel
            initDetail()
        }
    }

    fun setTaskList(taskList: List<TaskModel>) {
        view?.let {
            this.taskList.addAll(taskList)
            taskAdapter.setDataSource(taskList)
            shimmer_task.stopShimmer()
            shimmer_task.visibility = GONE
            recycler_task.visibility = VISIBLE
        }
    }

    fun setWeather(weather: WeatherModel) {
        view?.let {
            loading_weather.visibility = GONE
            layout_weather_detail.visibility = VISIBLE
            text_username.text = "Hello, ${prefManager.username}!"
            text_temperature.text = fahrenheitToCelsius((weather.currently.temperature).toInt())
            text_summary.text = weather.daily.summary
        }
    }

    fun initAnimate() {
        closeKeyboard(activity, edit_title.windowToken)
        closeKeyboard(activity, edit_description.windowToken)
        if (layout_detail.visibility == GONE) {
            layout_detail.bringToFront()
            layout_detail.visibility = VISIBLE
            recycler_task.startAnimation(outToLeftAnimation())
            layout_detail.startAnimation(inFromRightAnimation())
            recycler_task.visibility = GONE
            (activity as HomeActivity).initHeader()
        } else {
            recycler_task.bringToFront()
            recycler_task.visibility = VISIBLE
            layout_detail.startAnimation(outToRightAnimation())
            recycler_task.startAnimation(inFromLeftAnimation())
            layout_detail.visibility = GONE
            (activity as HomeActivity).initHeader()
            configTaskEdit()
        }
    }

    private fun initShimmer() {
        shimmer_task.startShimmer()
    }

    private fun initDetail() {
        selectedTask.value?.let {
            edit_title.setText(it.name, TextView.BufferType.EDITABLE)
            edit_description.setText(it.description, TextView.BufferType.EDITABLE)
            text_status.text = it.status
        }

    }

    private fun clearDetail() {
        edit_title.text.clear()
        edit_description.text.clear()
    }

    private fun configTaskEdit() {
        if (edit_title.text.toString() == selectedTask.value?.name
            && edit_description.text.toString() == selectedTask.value?.description
            && text_status.text == selectedTask.value?.status) {
            clearDetail()
            return
        }
        layout_bottom_loading.visibility = VISIBLE
        layout_bottom_loading.startAnimation(inFromBottomAnimation())
        val updateTaskModel = UpdateTaskModel(
            edit_title.text.toString(),
            edit_description.text.toString(),
            selectedTask.value?.order!!,
            selectedTask.value?.status!!)
        viewModel
            .updateTask(
                selectedTask.value?._id,
                updateTaskModel)
            .observe(this, Observer<TaskModel> {
                Logger.d("PUT task SUCCESS: $it")
                taskAdapter.removeTask(selectedTask.value?._id)
                taskAdapter.updateTask(it)
                layout_bottom_success.visibility = VISIBLE
                layout_bottom_success.startAnimation(inFromBottomAnimation())
                layout_bottom_loading.visibility = GONE
                loadHandler.postDelayed({
                    layout_bottom_success.visibility = GONE
                }, 1500)
            })
        viewModel
            .updateTaskError()
            .observe(this, Observer<String> {
                Logger.d("PUT task FAIL: $it")
                layout_bottom_loading.visibility = GONE
                layout_bottom_failed.visibility = VISIBLE
                layout_bottom_failed.startAnimation(inFromBottomAnimation())
                loadHandler.postDelayed({
                    layout_bottom_failed.visibility = GONE
                }, 1500)
            })
    }

    private fun addTask() {

    }
}