package com.adibsurani.aerotask.helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import com.adibsurani.aerotask.model.auth.LoginModel;

import static com.adibsurani.aerotask.helper.Constants.*;

@SuppressLint("CommitPrefEdits")
public class PrefManager extends Activity {

    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    public PrefManager(Context context) {
        this.context = context;
        sharedPref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPref.edit();
    }

    // local check if logged in
    public boolean isLoggedIn() {
        return sharedPref.getBoolean(IS_LOGIN, false);
    }

    // local setup login
    public void setupLogin(LoginModel loginModel) {
        editor.putBoolean(IS_LOGIN,true);
        editor.putString(USER_ID, loginModel.getUser().get_id());
        editor.putString(EMAIL, loginModel.getUser().getEmail());
        editor.putString(USERNAME, loginModel.getUser().getUsername());
        editor.putString(TOKEN, loginModel.getJwt());
        editor.commit();
    }

    // local setup logout
    public void setupLogout() {
        editor.clear();
        editor.commit();
        finish();
    }

    // local get logged ic_user item
    public String getUserID() {
        return sharedPref.getString(USER_ID, null);
    }
    public String getUsername() { return sharedPref.getString(USERNAME, null); }
    public String getEmail() {
        return sharedPref.getString(EMAIL, null);
    }
    public String getToken() {
        return sharedPref.getString(TOKEN, null);
    }
    public String getBearerToken() {
        return "Bearer " + getToken();
    }
}

