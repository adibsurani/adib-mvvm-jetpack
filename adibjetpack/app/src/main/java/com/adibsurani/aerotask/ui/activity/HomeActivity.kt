package com.adibsurani.aerotask.ui.activity

import android.Manifest
import android.content.pm.PackageManager
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.adibsurani.aerotask.R
import com.adibsurani.aerotask.helper.GPSTracker
import com.adibsurani.aerotask.helper.RecyclerUtil
import com.adibsurani.aerotask.helper.Utils
import com.adibsurani.aerotask.model.DrawerModel
import com.adibsurani.aerotask.model.TaskModel
import com.adibsurani.aerotask.model.weather.WeatherModel
import com.adibsurani.aerotask.ui.adapter.DrawerAdapter
import com.adibsurani.aerotask.ui.adapter.ViewPagerAdapter
import com.adibsurani.aerotask.ui.base.BaseActivity
import com.adibsurani.aerotask.ui.fragment.TaskFragment
import com.adibsurani.aerotask.viewmodel.TaskViewModel
import com.adibsurani.aerotask.viewmodel.WeatherViewModel
import com.jaeger.library.StatusBarUtil
import com.orhanobut.logger.Logger
import im.delight.android.location.SimpleLocation
import kotlinx.android.synthetic.main.activity_home.*
import qiu.niorgai.StatusBarCompat


class HomeActivity: BaseActivity() {

    private lateinit var viewModel: TaskViewModel
    private lateinit var weatherViewModel: WeatherViewModel
    private lateinit var drawerAdapter: DrawerAdapter
    private lateinit var viewPagerAdapter: ViewPagerAdapter
    private lateinit var gpsTracker: GPSTracker
    private lateinit var simpleLocation: SimpleLocation

    override fun initLayout(): Int {
        return R.layout.activity_home
    }

    override fun initCreate() {
        StatusBarUtil.setTransparent(this)
        gpsTracker = GPSTracker(this)
        simpleLocation = SimpleLocation(this)
        initViewModel()
        initPermission()
        initClick()
        initDrawer()
        initRecycler()
        initViewPager()
        initData()
    }

    private fun initData() {
        if (!Utils.isInternetConnected(this)) {
            return
        }
        getAllTask()
        getWeather()
    }

    private fun initClick() {
        image_menu.setOnClickListener {
            doDrawer()
        }
    }

    private fun initDrawer() {
        drawerAdapter = DrawerAdapter(this, this)
        val drawerItem = ArrayList<DrawerModel>()
        drawerItem.add(0, setDrawerItem(R.drawable.ic_logo, "Tasks"))
        drawerItem.add(1, setDrawerItem(R.drawable.ic_user, "User"))
        drawerItem.add(2, setDrawerItem(R.drawable.ic_delete, "Deleted"))
        drawerAdapter.setDataSource(drawerItem)
        recycler_drawer.adapter = drawerAdapter
    }

    fun selectDrawer(position: Int) {
        when (position) {
            0 -> {
                viewpager.currentItem = 0
            }
            1 -> {
                viewpager.currentItem = 0
            }
            2 -> {
                viewpager.currentItem = 0
            }
        }
        doDrawer()
    }

    private fun doDrawer() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers()
        } else {
            drawer.openDrawer(GravityCompat.START)
            drawer.bringToFront()
        }
    }

    private fun initRecycler() {
        RecyclerUtil.setupVertical(recycler_drawer, this)
    }

    private fun initViewPager() {
        viewPagerAdapter = ViewPagerAdapter(this,supportFragmentManager)
        viewPagerAdapter.addFragment(TaskFragment(), "Tasks")
        viewpager.adapter = viewPagerAdapter
        viewpager.offscreenPageLimit = 3
        viewpager.currentItem = 0
    }

    private fun initPermission() {
        askPermissions(
            arrayOf(
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            931
        )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isEmpty()) {
            return
        }
    }

    private fun askPermissions(permissions: Array<String>, requestCode: Int) {
        val permissionsToRequest = ArrayList<String>()
        for (permission in permissions) {
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsToRequest.add(permission)
            }
        }
        if (!permissionsToRequest.isEmpty()) {
            ActivityCompat.requestPermissions(this, permissionsToRequest.toTypedArray(), requestCode)
        } else {
        }
    }

    private fun setDrawerItem(icon: Int,
                              title: String): DrawerModel {
        return DrawerModel(icon, title)
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders
            .of(this)
            .get(TaskViewModel::class.java)
        weatherViewModel = ViewModelProviders
            .of(this)
            .get(WeatherViewModel::class.java)
    }

    private fun getAllTask() {
        viewModel
            .allTasks
            .observe(this, Observer<List<TaskModel>> {
                Logger.d("GET allTask SUCCESS: $it")
                (viewPagerAdapter.getItem(0) as TaskFragment)
                    .setTaskList(it)
            })
        viewModel
            .allTasksError()
            .observe(this, Observer<String> {
                Logger.e("GET allTask FAIL: $it")
            })
    }

    private fun getWeather() {
        if(!gpsTracker.canGetLocation()) {
            return
        }
        Logger.d("COORDINATE: ${gpsTracker.latitude},${gpsTracker.longitude}")
        weatherViewModel
            .getWeather(gpsTracker.latitude, gpsTracker.longitude)
            .observe(this, Observer<WeatherModel> {
                Logger.d("GET weather SUCCESS: $it")
                (viewPagerAdapter.getItem(0) as TaskFragment)
                    .setWeather(it)
            })
        weatherViewModel
            .weatherError
            .observe(this, Observer<String> {
                Logger.d("GET weather FAIL: $it")
            })
    }

    fun initHeader() {
        if(layout_header.visibility == GONE) {
            layout_header.visibility = VISIBLE
            StatusBarUtil.setTransparent(this)
            StatusBarUtil.setLightMode(this)
        } else {
            layout_header.visibility = GONE
            StatusBarUtil.setDarkMode(this)
            StatusBarCompat.setStatusBarColor(this, resources.getColor(R.color.colorPrimary))
        }
    }



    override fun onBackPressed() {
        if (viewpager.currentItem == 0) {
            (viewPagerAdapter.getItem(0) as TaskFragment)
                .initAnimate()
            return
        }
        super.onBackPressed()
    }
}