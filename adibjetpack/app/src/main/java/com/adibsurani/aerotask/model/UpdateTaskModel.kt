package com.adibsurani.aerotask.model

data class UpdateTaskModel(val name: String,
                           val description: String,
                           val order: Int,
                           val status: String)