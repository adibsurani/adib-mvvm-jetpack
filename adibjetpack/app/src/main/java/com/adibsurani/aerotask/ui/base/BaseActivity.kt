package com.adibsurani.aerotask.ui.base

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.adibsurani.aerotask.helper.PrefManager
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger


abstract class BaseActivity: AppCompatActivity() {

    lateinit var pref: PrefManager
    val loadHandler = Handler()
    protected abstract fun initLayout(): Int
    protected abstract fun initCreate()

    override fun onCreate (savedInstanceState: Bundle?) {
        super.onCreate (savedInstanceState)
        setContentView (initLayout())
        Logger.addLogAdapter(AndroidLogAdapter())
        pref = PrefManager(this)
        initCreate()
    }
}